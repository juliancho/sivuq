/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.jsf.bean;

import com.uniquindio.siviuq.jhs.bo.FacultadBO;
import java.io.Serializable;

import java.util.List;
import javax.annotation.PostConstruct;
/**
 *
 * @author SergioRios
 */
public class BeanFacultad implements Serializable{
    private static final long serialVersionUID = 7437375728438063174L;
   
    private List<BeanFacultad> lista;
    private String criterio;
    private String value;
    private FacultadBO facultadBO;
    private String idfacultad;
    private String nombre;
    private String campo;
    private String mensaje;
    
    
    public String insert() {
        getFacultadBO().insert(this);
        getAll();
        return "";
    }
    
    public String delete() {
        getFacultadBO().delete(this);
        getAll();
        return "";
    }
    
    public String update() {
        getFacultadBO().update(this);
        getAll();
        return "";
    }
    
    @PostConstruct
    public void getAll() {
        setLista(getFacultadBO().getAll());
        System.out.println(lista.get(0).getNombre()+ "llegue al getAll");
    }
    
    public void buscarByCriterio() {
        if(getCriterio().equals("begin")) {
            setCriterio("like");
            setValue(getValue()+"%");
        } else if(getCriterio().equals("end")) {
            setCriterio("like");
            setValue("%"+getValue());
        } else if(getCriterio().equals("content")) {
            setCriterio("like");
            setValue("%"+getValue()+"%");
        }
        setLista(getFacultadBO().getAllByCriterio(this));
    }

    public FacultadBO getFacultadBO() {
        return facultadBO;
    }

    public void setFacultadBO(FacultadBO facultadBO) {
        this.facultadBO = facultadBO;
    }

    public String getIdfacultad() {
        return idfacultad;
    }

    public void setIdfacultad(String idfacultad) {
        this.idfacultad = idfacultad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    
    
    /**
     * @return the lista
     */
    public List<BeanFacultad> getLista() {
        getAll();
        return lista;
    }

    /**
     * @param lista the lista to set
     */
    public void setLista(List<BeanFacultad> lista) {
        this.lista = lista;
    }

   
    /**
     * @return the criterio
     */
    public String getCriterio() {
        return criterio;
    }

    /**
     * @param criterio the criterio to set
     */
    public void setCriterio(String criterio) {
        this.criterio = criterio;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    public String getCampo() {
        return campo;
    }

    public void setCampo(String campo) {
        this.campo = campo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    
}
