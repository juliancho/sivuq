/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.jsf.bean;

import com.uniquindio.siviuq.jhs.bo.FacultadBO;
import com.uniquindio.siviuq.jhs.bo.NuevoUsuarioBO;
import com.uniquindio.siviuq.jhs.bo.UsuarioBO;
import com.uniquindio.siviuq.jhs.persistence.Facultad;
import com.uniquindio.siviuq.jhs.persistence.Investigador;
import com.uniquindio.siviuq.jhs.persistence.Programa;
import java.io.Serializable;
import java.util.ArrayList;

import java.util.List;
import java.util.Vector;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;
/**
 *
 * @author SergioRios
 */
public class BeanNuevoUsuario implements Serializable{
   
    private String mensaje;
    private Boolean status;
    private String idUsuario;
    private String clave;
    private String clavever;
    private String url;
    private String rol;
    private Facultad facultad;
    private Programa programa;
    private Investigador investigador;
    private List<BeanNuevoUsuario> listaUsuarios;
    private NuevoUsuarioBO nuevoUsuarioBO;
    private String criterio;
    private String value;
    private String campo;
    private String mensajeClave;
    private boolean combo=true;
    private List<String> elementoCombo=new ArrayList<String>();
    private List<SelectItem> miCombo =new ArrayList<SelectItem>();
    private String identificador;
    
    
     public void verificarClaves(){
        System.out.println("entro al if");
         if(!getClave().equals("") && !getClavever().equals("")){
             System.out.println("entro al if");
             if(clave.equals(clavever))
                 setMensajeClave( "confirmacion exitosa");
             else
                 setMensajeClave("las claves son diferentes");
         }
        
     }   
    
     public void llenarCombo(){
         System.out.println("llegue al rol..pase el combo");
         if(!rol.equals("")){
             if(rol.equals("Programa")){
                 BeanPrograma p=new BeanPrograma();
                 p.getAll();
                 List<BeanPrograma> l= p.getLista();
                 List<String> lista= new ArrayList<String>();
                 for(int i=0;i<l.size();i++){
                     lista.add(l.get(i).getNombre());
                 }
                 elementoCombo=lista;
             }
             
              if(rol.equals("Facultad")){
                 
                 BeanFacultad p=new BeanFacultad();
                 p.getAll();
                 System.out.println("pase el getAll" + p.getLista().get(0).getNombre());
                 List<BeanFacultad> l= p.getLista();
                 List<String> lista= new ArrayList<String>();
                 for(int i=0;i<l.size();i++){
                     System.out.println("entre al for"+l.get(i).getNombre());
                     lista.add(l.get(i).getNombre());
                 }
                 elementoCombo=lista;
             }
             
         }
     }
     
     public void crearSelectItems(){
        System.out.println("llegue al crearSelectIndex");
         llenarCombo();
         SelectItemGroup g = new SelectItemGroup(rol);
         ArrayList<SelectItem> obj= new ArrayList();
         SelectItem[] s = new SelectItem[elementoCombo.size()];
         for(int i=0;i>elementoCombo.size();i++){
            s[i]= new SelectItem(elementoCombo.get(i),elementoCombo.get(i));
         }
         
         g.setSelectItems(s);
         miCombo=new ArrayList<SelectItem>();
         miCombo.add(g);
         
     }
     
     
     public boolean guardarRol(){
         System.out.println("llegue al rol:::"+rol);
         if(rol.equals("Programa")||rol.equals("Facultad")||rol.equals("Investigador")){
                
                 combo=false;
                  
                 crearSelectItems();
             
                 return true;
             }       
         return false;
         
     }
     
     public String guardarIdentificador(){
         if(rol.equals("Programa")||rol.equals("Facultad")||rol.equals("Investigador")){
                 combo=false;
                 crearSelectItems();
                 return "";
             }       
         return "";
     }
     
    public String insert() {
        getNuevoUsuarioBO().insert(this);
    
        return "";
    }
    
    public String delete() {
        getNuevoUsuarioBO().delete(this);
        
        return "";
    }
    
    public String update() {
        getNuevoUsuarioBO().update(this);
       
        return "";
    }
    
    @PostConstruct
    public void getAll() {
        setListaUsuarios(getNuevoUsuarioBO().getAll());
        
    }
    
    public void buscarByCriterio() {
        if(getCriterio().equals("begin")) {
            setCriterio("like");
            setValue(getValue()+"%");
        } else if(getCriterio().equals("end")) {
            setCriterio("like");
            setValue("%"+getValue());
        } else if(getCriterio().equals("content")) {
            setCriterio("like");
            setValue("%"+getValue()+"%");
        }
        setListaUsuarios(getNuevoUsuarioBO().getAllByCriterio(this));
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getClavever() {
        return clavever;
    }

    public void setClavever(String clavever) {
        this.clavever = clavever;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public Facultad getFacultad() {
        return facultad;
    }

    public void setFacultad(Facultad facultad) {
        this.facultad = facultad;
    }

    public Programa getPrograma() {
        return programa;
    }

    public void setPrograma(Programa programa) {
        this.programa = programa;
    }

    public Investigador getInvestigador() {
        return investigador;
    }

    public void setInvestigador(Investigador investigador) {
        this.investigador = investigador;
    }

    public List<BeanNuevoUsuario> getListaUsuarios() {
        return listaUsuarios;
    }

    public void setListaUsuarios(List<BeanNuevoUsuario> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    public NuevoUsuarioBO getNuevoUsuarioBO() {
        return nuevoUsuarioBO;
    }

    public void setNuevoUsuarioBO(NuevoUsuarioBO usuarionuevoBO) {
        this.nuevoUsuarioBO = usuarionuevoBO;
    }

    public String getCriterio() {
        return criterio;
    }

    public void setCriterio(String criterio) {
        this.criterio = criterio;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCampo() {
        return campo;
    }

    public void setCampo(String campo) {
        this.campo = campo;
    }

    public String getMensajeClave() {
        return mensajeClave;
    }

    public void setMensajeClave(String mensajeClave) {
        this.mensajeClave = mensajeClave;
    }

    public boolean isCombo() {
        return combo;
    }

    public void setCombo(boolean combo) {
        this.combo = combo;
    }

    public List<String> getElementoCombo() {
        return elementoCombo;
    }

    public void setElementoCombo(List<String> elementoCombo) {
        this.elementoCombo = elementoCombo;
    }

    public List<SelectItem> getMiCombo() {
        return miCombo;
    }

    public void setMiCombo(List<SelectItem> miCombo) {
        this.miCombo = miCombo;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    
}
