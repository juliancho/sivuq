package com.uniquindio.siviuq.jsf.bean;

import com.uniquindio.siviuq.exception.SVException;
import com.uniquindio.siviuq.jhs.bo.UsuarioBO;
import com.uniquindio.siviuq.jhs.persistence.Facultad;
import com.uniquindio.siviuq.jhs.persistence.Investigador;
import com.uniquindio.siviuq.jhs.persistence.Programa;
import com.uniquindio.siviuq.util.Utilidades;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

public class BeanUsuario implements Serializable{
    
    private String mensaje;
    private Boolean status;
    private String idUsuario;
    private String clave;
    private String clavever;
    private String url;
    private String rol;
    private Facultad facultad;
    private Programa programa;
    private Investigador investigador;
    private List<BeanUsuario> listaUsuarios;
    private UsuarioBO usuarioBO;
    
    
        public String validarUsuario() {
            try{
            
            setClave(Utilidades.encriptPassword(getClave()));
            System.out.println("User : "+getIdUsuario());
            System.out.println("Password : "+getClave());
            getUsuarioBO().validaUsuario(this);
            System.out.println(this.getMensaje());
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            if(this.getStatus()) {
                Map<String, Object> sessionMap = externalContext.getSessionMap();
                setListaUsuarios(new ArrayList<BeanUsuario>());
                sessionMap.put("listaUsuarios", getListaUsuarios());
                System.out.println("Password : "+getClave());
                sessionMap.put("usuarioLogeado", this);
                return getUrl();
                } else {
                    ((HttpServletRequest)externalContext.getRequest()).setAttribute("errorAccesos", getMensaje());
                    return "login";
                }
            }catch(SVException ex){
                Utilidades.addMsgError("Error en validarLogin", ex.getMsgException());
            }
            return "login";
    }

    
     
     public void llenarCombo(){
         
     }
     
     
    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public Facultad getFacultad() {
        return facultad;
    }

    public void setFacultad(Facultad facultad) {
        this.facultad = facultad;
    }

    public Programa getPrograma() {
        return programa;
    }

    public void setPrograma(Programa programa) {
        this.programa = programa;
    }

    public Investigador getInvestigador() {
        return investigador;
    }

    public void setInvestigador(Investigador investigador) {
        this.investigador = investigador;
    }

    
    
    /**
     * @return the mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    /**
     * @return the status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * @return the user
     */
    
    public void setUrl(String url) {
        this.url = url;
    }
    
    public String getUrl() {
        return url;
    }
    
    /**
     * @return the listaUsuarios
     */
    public List<BeanUsuario> getListaUsuarios() {
        return listaUsuarios;
    }

    /**
     * @param listaUsuarios the listaUsuarios to set
     */
    public void setListaUsuarios(List<BeanUsuario> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }
    

    /**
     * @return the usuarioBO
     */
    public UsuarioBO getUsuarioBO() {
        return usuarioBO;
    }

    /**
     * @param loginBO the usuarioBO to set
     */
    public void setUsuarioBO(UsuarioBO usuarioBO) {
        this.usuarioBO = usuarioBO;
    }

    public String getClavever() {
        return clavever;
    }

    public void setClavever(String clavever) {
        this.clavever = clavever;
    }
    
    
}
