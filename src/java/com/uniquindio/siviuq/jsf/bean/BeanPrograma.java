/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.jsf.bean;

import com.uniquindio.siviuq.jhs.bo.ProgramaBO;
import com.uniquindio.siviuq.jhs.persistence.Facultad;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;

/**
 *
 * @author SergioRios
 */
public class BeanPrograma implements Serializable{
   
     private String idprograma;
     private Facultad facultad;
     private String nombre;
    
    private ProgramaBO programaBO;
    private List<BeanPrograma> lista;
    private Map<String, BigDecimal> mapa;

   
    public String insert() {
        programaBO.insert(this);
        getAll();
        return "";
    }
    
    public String delete() {
        programaBO.delete(this);
        getAll();
        return "";
    }
    
    public String update() {
        programaBO.update(this);
        getAll();
        return "";
    }
    
    public void getAll() {
        setLista(programaBO.getAll());
    }
    
    @PostConstruct
    public void llenaMapa() {
        /*
        getAll();
        mapa = new HashMap();
        for(BeanPrograma obj: getLista()) {
            mapa.put(obj.getIdprograma());
        }
    */
    }

    /**
     * @return the lista
     */
    public List<BeanPrograma> getLista() {
        return lista;
    }

    /**
     * @param lista the lista to set
     */
    public void setLista(List<BeanPrograma> lista) {
        this.lista = lista;
    }

    /**
     * @return the articuloBO
     */
    public ProgramaBO getArticuloBO() {
        return programaBO;
    }

    /**
     * @param articuloBO the articuloBO to set
     */
    public void setArticuloBO(ProgramaBO programaBO) {
        this.programaBO = programaBO;
    }

    /**
     * @return the mapa
     */
    public Map<String, BigDecimal> getMapa() {
        return mapa;
    }

    /**
     * @param mapa the mapa to set
     */
    public void setMapa(Map<String, BigDecimal> mapa) {
        this.mapa = mapa;
    }

    public String getIdprograma() {
        return idprograma;
    }

    public void setIdprograma(String idprograma) {
        this.idprograma = idprograma;
    }

    public Facultad getFacultad() {
        return facultad;
    }

    public void setFacultad(Facultad facultad) {
        this.facultad = facultad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ProgramaBO getProgramaBO() {
        return programaBO;
    }

    public void setProgramaBO(ProgramaBO programaBO) {
        this.programaBO = programaBO;
    }
    
    
}
