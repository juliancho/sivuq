/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.jhs.bo;

import com.uniquindio.siviuq.jdbc.dao.IfacePrograma;
import com.uniquindio.siviuq.jhs.persistence.Programa;
import com.uniquindio.siviuq.jsf.bean.BeanPrograma;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author SergioRios
 */
public class ProgramaImplBO implements ProgramaBO {
    private IfacePrograma programaDAO;
    
    @Override
    public void insert(BeanPrograma obj) {
        Programa programa = new Programa();
        programa.setIdprograma(obj.getIdprograma());
        programa.setNombre(obj.getNombre());
        programa.setFacultad(obj.getFacultad());
              
        getProgramaDAO().insert(programa);
    }

    @Override
    public void delete(BeanPrograma obj) {
        Programa programa = new Programa();
        programa.setIdprograma(obj.getIdprograma());
        programa.setNombre(obj.getNombre());
        programa.setFacultad(obj.getFacultad());
        
        getProgramaDAO().delete(programa);
    }

    @Override
    public void update(BeanPrograma obj) {
        Programa programa = new Programa();
        programa.setIdprograma(obj.getIdprograma());
        programa.setNombre(obj.getNombre());
        programa.setFacultad(obj.getFacultad());
        
        getProgramaDAO().update(programa);
    }

    @Override
    public List<BeanPrograma> getAll() {
        List<BeanPrograma> lista = new ArrayList();
        List<Programa> nuevaLista = programaDAO.getAll();
        for(Programa obj: nuevaLista) {
            BeanPrograma bean = new BeanPrograma();
            bean.setIdprograma(obj.getIdprograma());
            bean.setNombre(obj.getNombre());
            bean.setFacultad(obj.getFacultad());
           
            lista.add(bean);
        }
        return lista;
    }

    /**
     * @return the programaDAO
     */
    public IfacePrograma getProgramaDAO() {
        return programaDAO;
    }

    /**
     * @param articuloDAO the programaDAO to set
     */
    public void setArticuloDAO(IfacePrograma articuloDAO) {
        this.programaDAO = articuloDAO;
    }

    public void setProgramaDAO(IfacePrograma programaDAO) {
        this.programaDAO = programaDAO;
    }
    
}
