/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.jhs.bo;

import com.uniquindio.siviuq.jsf.bean.BeanFacultad;
import java.util.List;

/**
 *
 * @author SergioRios
 */
public interface FacultadBO {
    void insert(BeanFacultad obj);
    void delete(BeanFacultad obj);
    void update(BeanFacultad obj);
    List<BeanFacultad> getAll();
    List<BeanFacultad> getAllByFechas(BeanFacultad obj);
    List<BeanFacultad> getAllByCriterio(BeanFacultad obj);
}
