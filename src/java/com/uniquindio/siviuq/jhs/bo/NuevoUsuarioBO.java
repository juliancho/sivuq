/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.jhs.bo;

import com.uniquindio.siviuq.jsf.bean.BeanNuevoUsuario;
import java.util.List;

/**
 *
 * @author Juliancho
 */
public interface NuevoUsuarioBO {
     void insert(BeanNuevoUsuario obj);
    void delete(BeanNuevoUsuario obj);
    void update(BeanNuevoUsuario obj);
    List<BeanNuevoUsuario> getAll();
    List<BeanNuevoUsuario> getAllByFechas(BeanNuevoUsuario obj);
    List<BeanNuevoUsuario> getAllByCriterio(BeanNuevoUsuario obj);
}
