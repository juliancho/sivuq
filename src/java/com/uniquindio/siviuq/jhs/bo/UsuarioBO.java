/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.jhs.bo;

import com.uniquindio.siviuq.jhs.persistence.Usuario;
import com.uniquindio.siviuq.jsf.bean.BeanUsuario;
import java.util.List;

/**
 *
 * @author SergioRios
 */
public interface UsuarioBO {
    void validaUsuario(BeanUsuario obj);
   
}
