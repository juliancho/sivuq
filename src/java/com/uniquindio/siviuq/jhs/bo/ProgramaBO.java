/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.jhs.bo;

import com.uniquindio.siviuq.jsf.bean.BeanPrograma;
import java.util.List;

/**
 *
 * @author SergioRios
 */
public interface ProgramaBO {
    void insert(BeanPrograma obj);
    void delete(BeanPrograma obj);
    void update(BeanPrograma obj);
    List<BeanPrograma> getAll();
}
