/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.jhs.bo;

import com.uniquindio.siviuq.jdbc.dao.IfaceFacultad;
import com.uniquindio.siviuq.jhs.persistence.Facultad;
import com.uniquindio.siviuq.jsf.bean.BeanFacultad;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author SergioRios
 */
public class FacultadImplBO implements FacultadBO {
    private IfaceFacultad facultadDAO;
    
    @Override
    public void insert(BeanFacultad obj) {
        Facultad facultad = new Facultad();
        facultad.setIdfacultad(obj.getIdfacultad());
        facultad.setNombre(obj.getNombre());
        facultadDAO.insert(facultad);
        
    }

    @Override
    public void delete(BeanFacultad obj) {
        Facultad facultad = new Facultad();
        
        facultad.setIdfacultad(obj.getIdfacultad());
        facultad.setNombre(obj.getNombre());
        facultadDAO.delete(facultad);
    }

    @Override
    public void update(BeanFacultad obj) {
       Facultad facultad = new Facultad();
        facultad.setIdfacultad(obj.getIdfacultad());
        facultad.setNombre(obj.getNombre());
        
        facultadDAO.update(facultad);
    }

    @Override
    public List<BeanFacultad> getAll() {
        List<BeanFacultad> lista = new ArrayList();
        for(Facultad obj: facultadDAO.getAll()) {
            BeanFacultad bean = new BeanFacultad();
            bean.setIdfacultad(obj.getIdfacultad());
            bean.setNombre(obj.getNombre());
            
            lista.add(bean);
        }
        return lista;
    }

    @Override
    public List<BeanFacultad> getAllByFechas(BeanFacultad facultad) {
        List<BeanFacultad> lista = new ArrayList();
        for(Facultad obj: facultadDAO.getAllByFechas(facultad)) {
            BeanFacultad bean = new BeanFacultad();
            bean.setIdfacultad(obj.getIdfacultad());
            bean.setNombre(obj.getNombre());
           
            lista.add(bean);
        }
        return lista;
    }
    
    @Override
    public List<BeanFacultad> getAllByCriterio(BeanFacultad facultad) {
        List<BeanFacultad> lista = new ArrayList();
        for(Facultad obj: facultadDAO.buscarByCriterio(facultad)) {
            BeanFacultad bean = new BeanFacultad();
            bean.setIdfacultad(obj.getIdfacultad());
            bean.setNombre(obj.getNombre());
            lista.add(bean);
        }
        return lista;
    }

    public IfaceFacultad getFacultadDAO() {
        return facultadDAO;
    }

    public void setFacultadDAO(IfaceFacultad facultadDAO) {
        this.facultadDAO = facultadDAO;
    }
    
}
