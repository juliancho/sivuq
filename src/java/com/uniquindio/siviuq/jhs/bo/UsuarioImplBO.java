/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.jhs.bo;

import com.uniquindio.siviuq.jdbc.dao.ImplUsuario;
import com.uniquindio.siviuq.jhs.persistence.Usuario;
import com.uniquindio.siviuq.jsf.bean.BeanUsuario;
/**
 *
 * @author SergioRios
 */
public class UsuarioImplBO implements UsuarioBO {
    private ImplUsuario usuarioDAO;
    /**
     * @return the loginDAO
     */
    public ImplUsuario getUsuarioDAO() {
        return usuarioDAO;
    }

    /**
     * @param loginDAO the loginDAO to set
     */
    public void setUsuarioDAO(ImplUsuario loginDAO) {
        this.usuarioDAO = loginDAO;
    }
    
    @Override
    public void validaUsuario(BeanUsuario obj) {
        Usuario login = new Usuario();
        login.setIdusuario(obj.getIdUsuario());
        login.setClave(obj.getClave());
        login = getUsuarioDAO().validaUsuario(login);
        if(login != null) {
            obj.setUrl(login.getUrl());
            obj.setStatus(true);
            obj.setMensaje("Usuario encontrado");
        } else {
            obj.setStatus(false);
            obj.setMensaje("El usuario no existe");
        }
    }
}
