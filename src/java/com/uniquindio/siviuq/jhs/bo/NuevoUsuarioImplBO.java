/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.jhs.bo;

import com.uniquindio.siviuq.jdbc.dao.IfaceNuevoUsuario;
import com.uniquindio.siviuq.jhs.persistence.Usuario;
import com.uniquindio.siviuq.jsf.bean.BeanNuevoUsuario;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Juliancho
 */
public class NuevoUsuarioImplBO implements NuevoUsuarioBO{
     private IfaceNuevoUsuario nuevoUsuarioDAO;
    
    @Override
    public void insert(BeanNuevoUsuario obj) {
        Usuario u = new Usuario();
        u.setIdusuario(obj.getIdUsuario());
        u.setClave(obj.getClave());
        u.setRol(obj.getRol());
        u.setUrl(obj.getUrl());
        u.setFacultad(obj.getFacultad());
        u.setInvestigador(obj.getInvestigador());
        u.setPrograma(obj.getPrograma());
        
        nuevoUsuarioDAO.insert(u);
        
    }

    @Override
    public void delete(BeanNuevoUsuario obj) {
        Usuario usuario = new Usuario();
        
        usuario.setIdusuario(obj.getIdUsuario());
        
        nuevoUsuarioDAO.delete(usuario);
    }

    @Override
    public void update(BeanNuevoUsuario obj) {
       Usuario u = new Usuario();
        u.setIdusuario(obj.getIdUsuario());
        u.setClave(obj.getClave());
        u.setRol(obj.getRol());
        u.setUrl(obj.getUrl());
        u.setFacultad(obj.getFacultad());
        u.setInvestigador(obj.getInvestigador());
        u.setPrograma(obj.getPrograma());
        
        nuevoUsuarioDAO.update(u);
    }

    @Override
    public List<BeanNuevoUsuario> getAll() {
        List<BeanNuevoUsuario> lista = new ArrayList();
        for(Usuario obj: nuevoUsuarioDAO.getAll()) {
            System.out.println("llego este Ususario: "+obj.getIdusuario());
            BeanNuevoUsuario u = new BeanNuevoUsuario();
            u.setIdUsuario(obj.getIdusuario());
            u.setClave(obj.getClave());
            u.setRol(obj.getRol());
            u.setUrl(obj.getUrl());
            u.setFacultad(obj.getFacultad());
            u.setInvestigador(obj.getInvestigador());
            u.setPrograma(obj.getPrograma());
            
            lista.add(u);
        }
        return lista;
    }

    @Override
    public List<BeanNuevoUsuario> getAllByFechas(BeanNuevoUsuario usuario) {
        /*
        List<BeanNuevoUsuario> lista = new ArrayList();
        for(Usuario obj: nuevoUsuarioDAO.getAllByFechas(usuario)) {
            BeanNuevoUsuario u = new BeanNuevoUsuario();
            u.setIdUsuario(obj.getIdusuario());
            u.setClave(obj.getClave());
            u.setRol(obj.getRol());
            u.setUrl(obj.getUrl());
            u.setFacultad(obj.getFacultad());
            u.setInvestigador(obj.getInvestigador());
            u.setPrograma(obj.getPrograma());
           
            lista.add(u);
        }*/
        return null;
    }
    
    @Override
    public List<BeanNuevoUsuario> getAllByCriterio(BeanNuevoUsuario usuario) {
       /*
        List<BeanNuevoUsuario> lista = new ArrayList();
        for(Usuario obj: nuevoUsuarioDAO.buscarByCriterio(usuario)) {
            BeanNuevoUsuario u = new BeanNuevoUsuario();
           u.setIdUsuario(obj.getIdusuario());
            u.setClave(obj.getClave());
            u.setRol(obj.getRol());
            u.setUrl(obj.getUrl());
            u.setFacultad(obj.getFacultad());
            u.setInvestigador(obj.getInvestigador());
            u.setPrograma(obj.getPrograma());
            lista.add(u);
        }
        */return null;
    }

    public IfaceNuevoUsuario getNuevoUsuarioDAO() {
        return nuevoUsuarioDAO;
    }

    public void setNuevoUsuarioDAO(IfaceNuevoUsuario nuevoUsuarioDAO) {
        this.nuevoUsuarioDAO = nuevoUsuarioDAO;
    }

   
    
}
