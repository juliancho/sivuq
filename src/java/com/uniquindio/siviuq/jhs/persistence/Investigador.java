package com.uniquindio.siviuq.jhs.persistence;
// Generated 26-abr-2015 18:12:29 by Hibernate Tools 4.3.1


import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * Investigador generated by hbm2java
 */
public class Investigador  implements java.io.Serializable {


     private String idinvestigador;
     private Grupoinvestigacion grupoinvestigacion;
     private Programa programa;
     private String nombre;
     private String apellidos;
     private BigDecimal edad;
     private String telefonofijo;
     private String telefonocelular;
     private String correo;
     private String direccion;
     private Character principal;
     private String rol;
     private String tipovinculacion;
     private String curriculumruta;
     private Set proyectos = new HashSet(0);
     private Set titulos = new HashSet(0);
     private Set usuarios = new HashSet(0);

    public Investigador() {
    }

	
    public Investigador(String idinvestigador, Grupoinvestigacion grupoinvestigacion, Programa programa) {
        this.idinvestigador = idinvestigador;
        this.grupoinvestigacion = grupoinvestigacion;
        this.programa = programa;
    }
    public Investigador(String idinvestigador, Grupoinvestigacion grupoinvestigacion, Programa programa, String nombre, String apellidos, BigDecimal edad, String telefonofijo, String telefonocelular, String correo, String direccion, Character principal, String rol, String tipovinculacion, String curriculumruta, Set proyectos, Set titulos, Set usuarios) {
       this.idinvestigador = idinvestigador;
       this.grupoinvestigacion = grupoinvestigacion;
       this.programa = programa;
       this.nombre = nombre;
       this.apellidos = apellidos;
       this.edad = edad;
       this.telefonofijo = telefonofijo;
       this.telefonocelular = telefonocelular;
       this.correo = correo;
       this.direccion = direccion;
       this.principal = principal;
       this.rol = rol;
       this.tipovinculacion = tipovinculacion;
       this.curriculumruta = curriculumruta;
       this.proyectos = proyectos;
       this.titulos = titulos;
       this.usuarios = usuarios;
    }
   
    public String getIdinvestigador() {
        return this.idinvestigador;
    }
    
    public void setIdinvestigador(String idinvestigador) {
        this.idinvestigador = idinvestigador;
    }
    public Grupoinvestigacion getGrupoinvestigacion() {
        return this.grupoinvestigacion;
    }
    
    public void setGrupoinvestigacion(Grupoinvestigacion grupoinvestigacion) {
        this.grupoinvestigacion = grupoinvestigacion;
    }
    public Programa getPrograma() {
        return this.programa;
    }
    
    public void setPrograma(Programa programa) {
        this.programa = programa;
    }
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getApellidos() {
        return this.apellidos;
    }
    
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }
    public BigDecimal getEdad() {
        return this.edad;
    }
    
    public void setEdad(BigDecimal edad) {
        this.edad = edad;
    }
    public String getTelefonofijo() {
        return this.telefonofijo;
    }
    
    public void setTelefonofijo(String telefonofijo) {
        this.telefonofijo = telefonofijo;
    }
    public String getTelefonocelular() {
        return this.telefonocelular;
    }
    
    public void setTelefonocelular(String telefonocelular) {
        this.telefonocelular = telefonocelular;
    }
    public String getCorreo() {
        return this.correo;
    }
    
    public void setCorreo(String correo) {
        this.correo = correo;
    }
    public String getDireccion() {
        return this.direccion;
    }
    
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    public Character getPrincipal() {
        return this.principal;
    }
    
    public void setPrincipal(Character principal) {
        this.principal = principal;
    }
    public String getRol() {
        return this.rol;
    }
    
    public void setRol(String rol) {
        this.rol = rol;
    }
    public String getTipovinculacion() {
        return this.tipovinculacion;
    }
    
    public void setTipovinculacion(String tipovinculacion) {
        this.tipovinculacion = tipovinculacion;
    }
    public String getCurriculumruta() {
        return this.curriculumruta;
    }
    
    public void setCurriculumruta(String curriculumruta) {
        this.curriculumruta = curriculumruta;
    }
    public Set getProyectos() {
        return this.proyectos;
    }
    
    public void setProyectos(Set proyectos) {
        this.proyectos = proyectos;
    }
    public Set getTitulos() {
        return this.titulos;
    }
    
    public void setTitulos(Set titulos) {
        this.titulos = titulos;
    }
    public Set getUsuarios() {
        return this.usuarios;
    }
    
    public void setUsuarios(Set usuarios) {
        this.usuarios = usuarios;
    }




}


