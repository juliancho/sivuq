package com.uniquindio.siviuq.jhs.persistence;
// Generated 26-abr-2015 18:12:29 by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * Facultad generated by hbm2java
 */
public class Facultad  implements java.io.Serializable {


     private String idfacultad;
     private String nombre;
     private Set usuarios = new HashSet(0);
     private Set programas = new HashSet(0);

    public Facultad() {
    }

	
    public Facultad(String idfacultad) {
        this.idfacultad = idfacultad;
    }
    public Facultad(String idfacultad, String nombre, Set usuarios, Set programas) {
       this.idfacultad = idfacultad;
       this.nombre = nombre;
       this.usuarios = usuarios;
       this.programas = programas;
    }
   
    public String getIdfacultad() {
        return this.idfacultad;
    }
    
    public void setIdfacultad(String idfacultad) {
        this.idfacultad = idfacultad;
    }
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public Set getUsuarios() {
        return this.usuarios;
    }
    
    public void setUsuarios(Set usuarios) {
        this.usuarios = usuarios;
    }
    public Set getProgramas() {
        return this.programas;
    }
    
    public void setProgramas(Set programas) {
        this.programas = programas;
    }




}


