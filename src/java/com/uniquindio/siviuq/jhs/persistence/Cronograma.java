package com.uniquindio.siviuq.jhs.persistence;
// Generated 26-abr-2015 18:12:29 by Hibernate Tools 4.3.1


import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * Cronograma generated by hbm2java
 */
public class Cronograma  implements java.io.Serializable {


     private String idcronograma;
     private Proyecto proyecto;
     private BigDecimal cantidadmeses;
     private BigDecimal cantidadactividades;
     private Set actividads = new HashSet(0);

    public Cronograma() {
    }

	
    public Cronograma(String idcronograma, Proyecto proyecto) {
        this.idcronograma = idcronograma;
        this.proyecto = proyecto;
    }
    public Cronograma(String idcronograma, Proyecto proyecto, BigDecimal cantidadmeses, BigDecimal cantidadactividades, Set actividads) {
       this.idcronograma = idcronograma;
       this.proyecto = proyecto;
       this.cantidadmeses = cantidadmeses;
       this.cantidadactividades = cantidadactividades;
       this.actividads = actividads;
    }
   
    public String getIdcronograma() {
        return this.idcronograma;
    }
    
    public void setIdcronograma(String idcronograma) {
        this.idcronograma = idcronograma;
    }
    public Proyecto getProyecto() {
        return this.proyecto;
    }
    
    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }
    public BigDecimal getCantidadmeses() {
        return this.cantidadmeses;
    }
    
    public void setCantidadmeses(BigDecimal cantidadmeses) {
        this.cantidadmeses = cantidadmeses;
    }
    public BigDecimal getCantidadactividades() {
        return this.cantidadactividades;
    }
    
    public void setCantidadactividades(BigDecimal cantidadactividades) {
        this.cantidadactividades = cantidadactividades;
    }
    public Set getActividads() {
        return this.actividads;
    }
    
    public void setActividads(Set actividads) {
        this.actividads = actividads;
    }




}


