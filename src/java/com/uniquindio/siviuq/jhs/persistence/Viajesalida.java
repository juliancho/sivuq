package com.uniquindio.siviuq.jhs.persistence;
// Generated 26-abr-2015 18:12:29 by Hibernate Tools 4.3.1


import java.math.BigDecimal;

/**
 * Viajesalida generated by hbm2java
 */
public class Viajesalida  implements java.io.Serializable {


     private String idviaje;
     private Presupuesto presupuesto;
     private String lugar;
     private String justificacion;
     private BigDecimal costopasaje;
     private BigDecimal costoestadia;
     private BigDecimal numerodias;
     private BigDecimal valortotal;
     private Character viaje;
     private Character salidacampo;
     private BigDecimal rubro1;
     private BigDecimal rubro2;
     private BigDecimal rubro3;
     private BigDecimal rubro4;

    public Viajesalida() {
    }

	
    public Viajesalida(String idviaje) {
        this.idviaje = idviaje;
    }
    public Viajesalida(String idviaje, Presupuesto presupuesto, String lugar, String justificacion, BigDecimal costopasaje, BigDecimal costoestadia, BigDecimal numerodias, BigDecimal valortotal, Character viaje, Character salidacampo, BigDecimal rubro1, BigDecimal rubro2, BigDecimal rubro3, BigDecimal rubro4) {
       this.idviaje = idviaje;
       this.presupuesto = presupuesto;
       this.lugar = lugar;
       this.justificacion = justificacion;
       this.costopasaje = costopasaje;
       this.costoestadia = costoestadia;
       this.numerodias = numerodias;
       this.valortotal = valortotal;
       this.viaje = viaje;
       this.salidacampo = salidacampo;
       this.rubro1 = rubro1;
       this.rubro2 = rubro2;
       this.rubro3 = rubro3;
       this.rubro4 = rubro4;
    }
   
    public String getIdviaje() {
        return this.idviaje;
    }
    
    public void setIdviaje(String idviaje) {
        this.idviaje = idviaje;
    }
    public Presupuesto getPresupuesto() {
        return this.presupuesto;
    }
    
    public void setPresupuesto(Presupuesto presupuesto) {
        this.presupuesto = presupuesto;
    }
    public String getLugar() {
        return this.lugar;
    }
    
    public void setLugar(String lugar) {
        this.lugar = lugar;
    }
    public String getJustificacion() {
        return this.justificacion;
    }
    
    public void setJustificacion(String justificacion) {
        this.justificacion = justificacion;
    }
    public BigDecimal getCostopasaje() {
        return this.costopasaje;
    }
    
    public void setCostopasaje(BigDecimal costopasaje) {
        this.costopasaje = costopasaje;
    }
    public BigDecimal getCostoestadia() {
        return this.costoestadia;
    }
    
    public void setCostoestadia(BigDecimal costoestadia) {
        this.costoestadia = costoestadia;
    }
    public BigDecimal getNumerodias() {
        return this.numerodias;
    }
    
    public void setNumerodias(BigDecimal numerodias) {
        this.numerodias = numerodias;
    }
    public BigDecimal getValortotal() {
        return this.valortotal;
    }
    
    public void setValortotal(BigDecimal valortotal) {
        this.valortotal = valortotal;
    }
    public Character getViaje() {
        return this.viaje;
    }
    
    public void setViaje(Character viaje) {
        this.viaje = viaje;
    }
    public Character getSalidacampo() {
        return this.salidacampo;
    }
    
    public void setSalidacampo(Character salidacampo) {
        this.salidacampo = salidacampo;
    }
    public BigDecimal getRubro1() {
        return this.rubro1;
    }
    
    public void setRubro1(BigDecimal rubro1) {
        this.rubro1 = rubro1;
    }
    public BigDecimal getRubro2() {
        return this.rubro2;
    }
    
    public void setRubro2(BigDecimal rubro2) {
        this.rubro2 = rubro2;
    }
    public BigDecimal getRubro3() {
        return this.rubro3;
    }
    
    public void setRubro3(BigDecimal rubro3) {
        this.rubro3 = rubro3;
    }
    public BigDecimal getRubro4() {
        return this.rubro4;
    }
    
    public void setRubro4(BigDecimal rubro4) {
        this.rubro4 = rubro4;
    }




}


