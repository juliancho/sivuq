package com.uniquindio.siviuq.jhs.persistence;
// Generated 26-abr-2015 18:12:29 by Hibernate Tools 4.3.1


import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Convocatoria generated by hbm2java
 */
public class Convocatoria  implements java.io.Serializable {


     private String idconvocatoria;
     private String nombre;
     private Date fechapublicacion;
     private BigDecimal numeroresolucion;
     private String descripcionruta;
     private Set proyectos = new HashSet(0);

    public Convocatoria() {
    }

	
    public Convocatoria(String idconvocatoria) {
        this.idconvocatoria = idconvocatoria;
    }
    public Convocatoria(String idconvocatoria, String nombre, Date fechapublicacion, BigDecimal numeroresolucion, String descripcionruta, Set proyectos) {
       this.idconvocatoria = idconvocatoria;
       this.nombre = nombre;
       this.fechapublicacion = fechapublicacion;
       this.numeroresolucion = numeroresolucion;
       this.descripcionruta = descripcionruta;
       this.proyectos = proyectos;
    }
   
    public String getIdconvocatoria() {
        return this.idconvocatoria;
    }
    
    public void setIdconvocatoria(String idconvocatoria) {
        this.idconvocatoria = idconvocatoria;
    }
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public Date getFechapublicacion() {
        return this.fechapublicacion;
    }
    
    public void setFechapublicacion(Date fechapublicacion) {
        this.fechapublicacion = fechapublicacion;
    }
    public BigDecimal getNumeroresolucion() {
        return this.numeroresolucion;
    }
    
    public void setNumeroresolucion(BigDecimal numeroresolucion) {
        this.numeroresolucion = numeroresolucion;
    }
    public String getDescripcionruta() {
        return this.descripcionruta;
    }
    
    public void setDescripcionruta(String descripcionruta) {
        this.descripcionruta = descripcionruta;
    }
    public Set getProyectos() {
        return this.proyectos;
    }
    
    public void setProyectos(Set proyectos) {
        this.proyectos = proyectos;
    }




}


