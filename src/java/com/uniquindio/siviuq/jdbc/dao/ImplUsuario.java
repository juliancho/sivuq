package com.uniquindio.siviuq.jdbc.dao;


import com.uniquindio.siviuq.jhs.persistence.Usuario;
import java.util.List;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Transactional;

public class ImplUsuario extends HibernateDaoSupport implements IfaceUsuario {
   
    @Override
    public Usuario validaUsuario(Usuario obj) {
        List list = getHibernateTemplate().find("from Usuario where idusuario = ? and clave = ?", 
            obj.getIdusuario(), obj.getClave());
        if(list.size() > 0) {
            return (Usuario)list.get(0);
        }
        return null;
    }
    
   
}
