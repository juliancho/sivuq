/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.jdbc.dao;


import com.uniquindio.siviuq.jhs.persistence.Facultad;
import com.uniquindio.siviuq.jsf.bean.BeanFacultad;
import java.util.List;

/**
 *
 * @author SergioRios
 */
public interface IfaceFacultad {
    void insert(Facultad obj);
    void delete(Facultad obj);
    void update(Facultad obj);
    List<Facultad> getAll();
    List<Facultad> getAllByFechas(BeanFacultad bean);
    List<Facultad> getAllByNombre(BeanFacultad bean);
    List<Facultad> buscarByCriterio(BeanFacultad bean);
}
