/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.jdbc.dao;


import com.uniquindio.siviuq.jhs.persistence.Programa;
import java.util.List;

/**
 *
 * @author SergioRios
 */
public interface IfacePrograma {
    void insert(Programa obj);
    void delete(Programa obj);
    void update(Programa obj);
    List<Programa> getAll();
}
