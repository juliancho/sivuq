/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.jdbc.dao;


import com.uniquindio.siviuq.jhs.persistence.Facultad;
import com.uniquindio.siviuq.jsf.bean.BeanFacultad;
import java.util.Date;
import java.util.List;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author SergioRios
 */
public class ImplFacultad extends HibernateDaoSupport implements IfaceFacultad {
   
    @Override
    @Transactional
    public void insert(Facultad obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    @Transactional
    public void delete(Facultad obj) {
        getHibernateTemplate().delete(obj);
    }

    @Override
    @Transactional
    public void update(Facultad obj) {
        getHibernateTemplate().merge(obj);
    }

    @Override
    @Transactional
    public List getAll() {
        return getHibernateTemplate().find("from Facultad");
    }
    
    private java.sql.Date convertFecha(Date fecha) {
        return new java.sql.Date(fecha.getTime());
    }

    @Override
    @Transactional
    public List getAllByFechas(BeanFacultad bean) {
        /*
        String query = "from Alumnos al where al.fechaRegistro between '"+
        convertFecha(bean.getFecha1())+"' and '"+convertFecha(bean.getFecha2())+"'";
        System.out.println("Query by fechas : "+query);
        return getHibernateTemplate().find(query);
                */
        return null;
    }

    @Override
    @Transactional
    public List getAllByNombre(BeanFacultad bean) {
        String query = "from Facultad al where al.nombre = '"+bean.getNombre()+"'";
        System.out.println("Query by Nombre : "+query);
        return getHibernateTemplate().find(query);
    }
    
    @Override
    @Transactional
    public List buscarByCriterio(BeanFacultad bean) {
        String query = "from Facultad al where al." +bean.getCampo()+" "+bean.getCriterio()+" '"+bean.getValue()+"'";
        System.out.println("Query by "+bean.getNombre()+" : "+query);
        return getHibernateTemplate().find(query);
    }
}
