/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.jdbc.dao;


import com.uniquindio.siviuq.jhs.persistence.Programa;
import java.util.List;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author SergioRios
 */
public class ImplPrograma extends HibernateDaoSupport implements IfacePrograma {

    @Override
    @Transactional
    public void insert(Programa obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    @Transactional
    public void delete(Programa obj) {
        getHibernateTemplate().delete(obj);
    }

    @Override
    @Transactional
    public void update(Programa obj) {
        getHibernateTemplate().merge(obj);
    }

    @Override
    @Transactional
    public List getAll() {
        return getHibernateTemplate().find("from Programa");
    }
}
