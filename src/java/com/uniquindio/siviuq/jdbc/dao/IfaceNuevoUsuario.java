/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.jdbc.dao;

import com.uniquindio.siviuq.jhs.persistence.Usuario;
import com.uniquindio.siviuq.jsf.bean.BeanNuevoUsuario;
import java.util.List;

/**
 *
 * @author Juliancho
 */
public interface IfaceNuevoUsuario {
    void insert(Usuario obj);
    void delete(Usuario obj);
    void update(Usuario obj);
    List<Usuario> getAll();
   
    List<Usuario> getAllByKey(BeanNuevoUsuario bean);
    List<Usuario> buscarByCriterio(BeanNuevoUsuario bean);
}
