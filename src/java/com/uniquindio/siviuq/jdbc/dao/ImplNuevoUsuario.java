/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.jdbc.dao;

import com.uniquindio.siviuq.jhs.persistence.Usuario;
import com.uniquindio.siviuq.jsf.bean.BeanNuevoUsuario;
import java.util.Date;
import java.util.List;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Juliancho
 */
public class ImplNuevoUsuario extends HibernateDaoSupport implements IfaceNuevoUsuario{
    
    @Override
    @Transactional
    public void insert(Usuario obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    @Transactional
    public void delete(Usuario obj) {
        getHibernateTemplate().delete(obj);
    }

    @Override
    @Transactional
    public void update(Usuario obj) {
        getHibernateTemplate().merge(obj);
    }

    @Override
    @Transactional
    public List getAll() {
        return getHibernateTemplate().find("from Usuario");
    }
    
    private java.sql.Date convertFecha(Date fecha) {
        return new java.sql.Date(fecha.getTime());
    }

   

    @Override
    @Transactional
    public List getAllByKey(BeanNuevoUsuario bean) {
        String query = "from Usuario al where al.idusuario = '"+bean.getIdUsuario()+"'";
        System.out.println("Query by Nombre : "+query);
        return getHibernateTemplate().find(query);
    }
    
    @Override
    @Transactional
    public List buscarByCriterio(BeanNuevoUsuario bean) {
        String query = "from Usuario al where al." +bean.getCampo()+" "+bean.getCriterio()+" '"+bean.getValue()+"'";
        System.out.println("Query by "+bean.getIdUsuario()+" : "+query);
        return getHibernateTemplate().find(query);
    }
}
